package com.sample.rabbitmq;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Receiver {

    private final Logger logger = LoggerFactory.getLogger(Receiver.class);

    public void handleMessage(String text) throws Exception {
        Thread.sleep(1000);
        logger.info("Received: " + text);
//         throw new RuntimeException("exception");
    }

}
