package com.sample.rabbitmq.consumer;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConsumerLauncher {


    public static void main(String[] args) throws InterruptedException {

        new AnnotationConfigApplicationContext(ConsumerApplication.class);
        Thread.sleep(10000);
    }

}
