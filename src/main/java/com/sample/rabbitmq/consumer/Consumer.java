package com.sample.rabbitmq.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class Consumer implements MessageListener {


    @Override
    public void onMessage(Message message) {


        System.out.println(new String(message.getBody()));
        //This exception will cause the message to not be requeued and send to the dead letter
        //throw new AmqpRejectAndDontRequeueException("Exception");

        //Unchecked exception will cause the message to be requeued and replay
        //throw new RuntimeException("Exception");

    }
}