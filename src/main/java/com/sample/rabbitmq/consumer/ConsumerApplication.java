package com.sample.rabbitmq.consumer;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ConsumerApplication {

    private final String QUEUE_NAME = "myQueue";

    private final String DEAD_LETTER_QUEUE = "myQueue-dead_letter";

    private final String DEFAULT_EXCHANGE = "myExchange";

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("dev");
        connectionFactory.setPassword("dev");
        return connectionFactory;
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    /**
     * RabbitTemplate declaration, provide methods to interact with rabbitMQ
     *
     * @return
     */
    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMandatory(true);
        template.setMessageConverter(new SimpleMessageConverter());
        //The routing key is set to the name of the queue by the broker for the default exchange.
        template.setRoutingKey(this.QUEUE_NAME);
        template.setExchange(DEFAULT_EXCHANGE);
        //Where we will synchronously receive messages from
        template.setQueue(QUEUE_NAME);

        return template;
    }

    /**
     * Exchange declaration, route the message to the right queue
     *
     * @return
     */
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(DEFAULT_EXCHANGE);
    }

    /**
     * Queue binding declaration, binding the exchange to the queue
     *
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME);
    }

    /**
     * Dead letter binding declaration, binding the exchange to the queue
     *
     * @return
     */
    @Bean
    Binding bindingDeadLetter(Queue deadLetterQueue, TopicExchange exchange) {
        return BindingBuilder.bind(deadLetterQueue).to(exchange).with(DEAD_LETTER_QUEUE);
    }

    /**
     * Dead letter queue declaration
     *
     * @return
     */
    @Bean
    Queue deadLetterQueue() {
        return new Queue(DEAD_LETTER_QUEUE, true);
    }


    /**
     * Creating a queue with a dead letter
     * <p>
     * The following messages will be send to the dead letter queue
     * The message is rejected (basic.reject or basic.nack) with requeue=false,
     * The TTL for the message expires; or
     * The queue length limit is exceeded.
     * <p>
     *
     * @return
     */
    @Bean
    Queue queue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", DEFAULT_EXCHANGE);
        args.put("x-dead-letter-routing-key", DEAD_LETTER_QUEUE);
        //TTL
        args.put("x-message-ttl", 5000);
        return new Queue(QUEUE_NAME, true, false, false, args);
    }

    /**
     * Declare the listener, the {@link Consumer} will be the class used to receive messages
     *
     * @return
     */
    @Bean
    public SimpleMessageListenerContainer listenerContainer() {

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory());
        container.setQueueNames(this.QUEUE_NAME);
        container.setMessageConverter(jsonMessageConverter());
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setMessageListener(new Consumer());
        return container;
    }


}
