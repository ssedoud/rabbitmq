package com.sample.rabbitmq.producer;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    @Autowired
    private volatile RabbitTemplate rabbitTemplate;

    void sendMessage() throws InterruptedException {
            rabbitTemplate.convertAndSend(new CustomMessage(1, "message"));
    }

}
