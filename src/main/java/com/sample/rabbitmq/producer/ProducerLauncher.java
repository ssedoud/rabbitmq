package com.sample.rabbitmq.producer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ProducerLauncher {
    public static void main(String[] args) throws InterruptedException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ProducerApplication.class);
        Producer producer = context.getBean(Producer.class);
        producer.sendMessage();
    }
}
