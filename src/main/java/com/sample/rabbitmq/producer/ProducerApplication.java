package com.sample.rabbitmq.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProducerApplication {

    private final String QUEUE_NAME = "myQueue";

    private final String DEFAULT_EXCHANGE = "myExchange";

    private final Logger logger = LoggerFactory.getLogger(ProducerApplication.class);


    @Bean
    public Producer producer() {
        return new Producer();
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMandatory(true);
        template.setMessageConverter(jsonMessageConverter());

        template.setReturnCallback(
                (message, i, s, s1, s2) ->
                        logger.info("returned message")
        );

        template.setConfirmCallback((correlationData, ack, cause) -> {
//            if (correlationData != null) {
            logger.info("Received " + (ack ? " ack " : " nack ") + "for correlation: " + correlationData);
//            }
        });


        template.setRoutingKey(QUEUE_NAME);
        template.setExchange(DEFAULT_EXCHANGE);
        return template;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("dev");
        connectionFactory.setPassword("dev");
        connectionFactory.setPublisherReturns(true);
        connectionFactory.setPublisherConfirms(true);

        return connectionFactory;
    }

}
